package id.branditya.chapter3binartugas1

import android.content.pm.ActivityInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Log.e("activity lifecycle", "onCreate")
        val btn = findViewById<Button>(R.id.btn)
        var isPortrait = true
        btn.setOnClickListener {
            requestedOrientation = if (isPortrait) {
                ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
            } else {
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
            }

            isPortrait = !isPortrait
        }
    }

    override fun onStart() {
        super.onStart()
        Log.e("activity lifecycle", "onStart")
    }

    override fun onResume() {
        super.onResume()
        Log.e("activity lifecycle", "onResume")
    }

    override fun onRestart() {
        super.onRestart()
        Log.e("activity lifecycle", "onRestart")
    }

    override fun onPause() {
        super.onPause()
        Log.e("activity lifecycle", "onPause")
    }

    override fun onStop() {
        super.onStop()
        Log.e("activity lifecycle", "onStop")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.e("activity lifecycle", "onDestroy")
    }
}

/**
 * Urutan Method Activity Lifecycle
 * Saat pertama aplikasi dibuka:
 * -onCreate
 * -onStart
 * -onResume
 * Saat diubah rotasi:
 * -onPause
 * -onStop
 * -onDestroy
 * -onCreate
 * -onStart
 * -onResume
 */